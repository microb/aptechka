//
//  Strings.swift
//  MobileCommunity
//
//  Created by Anton Muratov on 2/1/16.
//  Copyright © 2016 Micro-b. All rights reserved.
//

struct ErrorInfo {
    static let Domain = "net.micro-b.aptechka"
}

/** Name of the storyboards */
struct Storyboards {
    static let Main = "Main"
    static let Registration = "Registration"
}

/** Name of the view controllers */
struct ViewControllers {
    static let AuthorizationViewController = "AuthorizationViewController"
    static let MedicamentsListViewController = "MedicamentsListViewController"
    static let MedicamentInfoViewController = "MedicamentInfoViewController"
    static let MedicamentViewController = "MedicamentViewController"
}

/** Name of images in App */
struct Images {
    
    struct Common {
        static let BackButton = "button_back"
    }
    
    struct MedicamentsList {
        static let PlusButton = "plus_fab_image"
    }
}

/** Name of the identifiers of the UITableView cells */
struct TableViewCellsIdentifiers {
    static let MedicamentCell = "MedicamentCell"
}

/** Titles of alert controllers */
struct AlertTitles {

}

/** Messages of alert controllers */
struct AlertMessages {
    static let InvalidResponse = "Invalid response"
    static let FieldsNotFilled = "All fields should be filled"
    static let InvalidEmail = "Invalid email"
    static let PasswordDontMatch = "Passwords do not match, please retype"
}

/** Names of the segues **/
struct Segues {
    static let ShowMedicamentInfoSegue = "ShowMedicamentInfoSegue"
    static let ShowMedicamentEditingSegue = "ShowMedicamentEditingSegue"
    static let BackToMedicamentListSegue = "BackToMedicamentListSegue"
    static let ShowMedicamentSegue = "ShowMedicamentSegue"
    static let PresentMainFromAuthorizeSegue = "PresentMainFromAuthorizeSegue"
    static let PresentMainFromRegistrationSegue = "PresentMainFromRegistrationSegue"
    static let PresentAuthorizationSegue = "PresentAuthorizationSegue"
}

struct Keys {
    static let Id = "id"
    static let UserId = "user_id"
    static let Name = "name"
    static let ExpirationDate = "expiration_date"
    static let Amount = "amount"
    static let Category = "category"
}















