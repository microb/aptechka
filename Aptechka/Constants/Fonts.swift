//
//  Fonts.swift
//  MobileCommunity
//
//  Created by Anton Muratov on 2/1/16.
//  Copyright © 2016 Micro-b. All rights reserved.
//

struct Fonts {
    static let Helvetica = "Helvetica"
    static let HelveticaBold = "Helvetica-Bold"
    static let FuturaOrtoTitul = "a_FuturaOrtoTitulInln"
}
