//
//  Colors.swift
//  MobileCommunity
//
//  Created by Anton Muratov on 2/1/16.
//  Copyright © 2016 Micro-b. All rights reserved.
//

struct Colors {
    static let NavigationBarColor = "0B5B55"
    static let StatusBarColor = "094B46"
}
