//
//  AppDelegate.swift
//  MobileCommunity
//
//  Created by Vadim Osovets on 1/24/16.
//  Copyright © 2016 Micro-b. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    // MARK: - Lifecycle
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        FIRApp.configure()
        // Set status bar style
        application.setStatusBarStyle(.LightContent, animated: false)
        
        // Store cached data even after the application is reloaded
        Firebase.defaultConfig().persistenceEnabled = true
        
        // Determine start screen in according to user authorization status
        HTTPClient.isAuthorized { (response, error) -> Void in
            let isAuthorized = response as! Bool
            
            if isAuthorized && HTTPClient.currentUserUid != nil {
                self.startWithMedicamentsListScreen()
            } else {
                self.startWithAuthorizationScreen()
            }
        }
        
        return true
    }
    
    
    // MARK: - Private
    
    private func startWithAuthorizationScreen() {
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        let registrationStoryboard = UIStoryboard(name: Storyboards.Registration, bundle: nil)
        let authorizationViewController = registrationStoryboard.instantiateInitialViewController()
        
        window?.rootViewController = authorizationViewController
        window?.makeKeyAndVisible()
    }
    
    private func startWithMedicamentsListScreen() {
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        let mainStoryboard = UIStoryboard(name: Storyboards.Main, bundle: nil)
        let medicamentsListViewController = mainStoryboard.instantiateInitialViewController()
        
        window?.rootViewController = medicamentsListViewController
        window?.makeKeyAndVisible()
    }
}
