//
//  HTTPClient.swift
//  Aptechka
//
//  Created by Anton Muratov on 3/14/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import Foundation
import Firebase

typealias Completion = (error: NSError?) -> Void
typealias CompletionWithResponse = (response: Any?, error: NSError?) -> Void

class HTTPClient {
    
    private static let firebaseUrl = "https://aptechka.firebaseio.com"
    private static let uidKey = "uid"
    
    static let baseReference: Firebase = {
        return Firebase(url: firebaseUrl)
    }()
    
    var baseReference = FIRDatabase.database().reference()
    
    static let medicamentsReference: Firebase = {
        return Firebase(url: firebaseUrl + "/medicaments")
    }()

    static let currentUserUid: String? = {
       return NSUserDefaults.standardUserDefaults().valueForKey(uidKey) as? String
    }()
    
    static func saveUserUid(uid: String) {
        NSUserDefaults.standardUserDefaults().setValue(uid, forKey: uidKey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    static func clearUserUid() {
        NSUserDefaults.standardUserDefaults().removeObjectForKey(uidKey)
    }
}
