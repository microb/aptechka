//
//  HTTPClient+Auth.swift
//  Aptechka
//
//  Created by Anton Muratov on 3/14/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import Firebase

extension HTTPClient {
    
    static func registerUser(email: String, password: String, completion: Completion) {
        baseReference.createUser(email, password: password) { (error) -> Void in
            completion(error: error)
        }
    }
    
    static func loginUser(email: String, password: String, completion: Completion) {
        baseReference.authUser(email, password: password) { (error, authData) -> Void in
            if let uid = authData?.uid {
                saveUserUid(uid)
            }
            completion(error: error)
        }
    }
    
    static func logoutUser(completion: Completion) {
        baseReference.unauth()
        clearUserUid()
        
        completion(error: nil)
    }
    
    static func isAuthorized(completion: CompletionWithResponse) {
        let authHandler =  baseReference.observeAuthEventWithBlock { (authData) -> Void in
            if authData != nil {
                completion(response: true, error: nil)
            } else {
                completion(response: false, error: nil)
            }
        }
        baseReference.removeAuthEventObserverWithHandle(authHandler)
    }
    
}
