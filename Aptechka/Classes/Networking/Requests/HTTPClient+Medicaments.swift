//
//  HTTPClient+Medicaments.swift
//  Aptechka
//
//  Created by Anton Muratov on 3/14/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import Firebase

extension HTTPClient {
    
    static func fetchMedicaments(completion: CompletionWithResponse) {
        medicamentsReference.queryOrderedByChild(Keys.UserId)
            .queryEqualToValue(HTTPClient.currentUserUid)
            .observeSingleEventOfType(.Value, withBlock: { snapshot in
                
                if snapshot.value is NSNull {
                    // Create error
                    let errorUserInfo = [NSLocalizedDescriptionKey : AlertMessages.InvalidResponse]
                    let error = NSError(domain: ErrorInfo.Domain, code: 0, userInfo: errorUserInfo)
                    
                    completion(response: nil, error: error)
                } else {
                    if !snapshot.hasChildren() {
                        completion(response: nil, error: nil)
                        return
                    }
                    
                    print("\(snapshot.value)")
                    
                    var medicaments = [Medicament]()
                    
                    for medicamentSnapshot in snapshot.children.allObjects as! [FDataSnapshot] {
                        if let medicament = Medicament.createWithSnapshot(medicamentSnapshot) {
                            medicaments.append(medicament)
                        }
                    }
                    completion(response: medicaments, error: nil)
                }
            })
    }

    static func createMedicament(medicament: Medicament, completion: Completion) {
        let medicamentInfo: NSMutableDictionary = [
            Keys.Category : medicament.category,
            Keys.Amount : medicament.amount,
            Keys.ExpirationDate : medicament.expirationDate,
            Keys.Name : medicament.name,
            Keys.UserId: currentUserUid!
        ]
        
        // childByAutoId automatically generates id for medicament
        let medicamentReferenceWithAutoId = medicamentsReference.childByAutoId()
        
        // Add generated id as a medicament's value
        medicamentInfo.setObject(medicamentReferenceWithAutoId.key, forKey: Keys.Id)

        medicamentReferenceWithAutoId.setValue(medicamentInfo) { (error, firebase) -> Void in
            completion(error: error)
        }
    }
    
    static func updateMedicament(medicament: Medicament, completion: Completion) {
        let medicamentNewInfo: NSMutableDictionary = [
            Keys.Category : medicament.category,
            Keys.Amount : medicament.amount,
            Keys.ExpirationDate : medicament.expirationDate,
            Keys.Name : medicament.name,
        ]
        
        // Get reference of existing medicament by its id
        let medicamentReference = medicamentsReference.childByAppendingPath(medicament.id)
        
        medicamentReference.updateChildValues(medicamentNewInfo as [NSObject : AnyObject]) { (error, firebase) -> Void in
            completion(error: error)
        }
    }
    
    static func deleteMedicament(medicament: Medicament, completion: Completion) {
        let medicamentReference = medicamentsReference.childByAppendingPath(medicament.id)
        
        medicamentReference.removeValueWithCompletionBlock { (error, firebase) -> Void in
            completion(error: error)
        }
    }
}
