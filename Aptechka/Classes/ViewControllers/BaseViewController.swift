//
//  ViewController.swift
//  Aptechka
//
//  Created by Anton Muratov on 3/2/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import UIKit
import Material

class BaseViewController: UIViewController {

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        setupStatusBar()
        
        if navigationController?.viewControllers.count > 1 {
            showBackButton()
        } else {
            hideBackButton()
        }
    }
    
    // MARK: - Private
    
    private func setupNavigationBar() -> Void {
        navigationController?.navigationBar.barTintColor = UIColor.colorWithHexString(Colors.NavigationBarColor)
        navigationController?.navigationBar.translucent = false
        navigationController?.navigationBar.setBottomBorderColor(UIColor.colorWithHexString(Colors.NavigationBarColor), height: 1)
        navigationController?.navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(name: Fonts.Helvetica, size: 16)!,
            NSForegroundColorAttributeName : UIColor.whiteColor()]
    }
    
    private func setupStatusBar() -> Void {
        let statusBarFrame : CGRect = CGRectMake(0, -20, UIScreen.mainScreen().bounds.size.width,
            UIApplication.sharedApplication().statusBarFrame.size.height)
        
        let statusBarView : UIView = UIView.init(frame: statusBarFrame)
        statusBarView.backgroundColor = UIColor.colorWithHexString(Colors.StatusBarColor)
        navigationController?.navigationBar.addSubview(statusBarView)
    }
    
    private func showBackButton() {
        let backButtonImage = UIImage(named: Images.Common.BackButton)
        
        let backButtonBarItem = UIBarButtonItem(
            image: backButtonImage,
            style: .Plain,
            target: navigationController,
            action: Selector("popViewControllerAnimated:"))
        
        backButtonBarItem.tintColor = UIColor.whiteColor()
        
        navigationItem.leftBarButtonItem = backButtonBarItem
    }
    
    private func hideBackButton() {
        navigationItem.leftBarButtonItem = nil
    }
}
