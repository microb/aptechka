//
//  MedicamentDescriptionViewController.swift
//  Aptechka
//
//  Created by Maxim Kamenev on 3/2/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import UIKit
import Material

enum MedicamentViewControllerMode {
    case Adding
    case Editing
}

class MedicamentViewController: BaseViewController {

    @IBOutlet weak private var titleTextField: TextField!
    @IBOutlet weak private var expirationDateTextField: TextField!
    @IBOutlet weak private var amountTextField: TextField!
    @IBOutlet weak private var categoryTextField: TextField!
    
    private var controllerMode = MedicamentViewControllerMode.Adding
    
    var medicament: Medicament?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTextFields()
        
        // If medicament isn't nill - controller is in editing mode
        if let medicament = medicament {
            setupControllerForEditingMedicament(medicament)
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func addButtonDidTap(sender: AnyObject) {
        if controllerMode == .Editing {
            editMedicament()
        } else if controllerMode == .Adding {
            addMedicament()
        }
    }
    
    @IBAction func viewDidTap(sender: AnyObject) {
        view.endEditing(true)
    }

    // MARK: - Private
    
    private func setupControllerForEditingMedicament(medicament: Medicament) {
        controllerMode = .Editing
        
        titleTextField.text = medicament.name
        expirationDateTextField.text = medicament.expirationDate
        amountTextField.text = String(medicament.amount)
        categoryTextField.text = medicament.category
    }
    
    private func setupTextFields() {
        TextField.setupMaterialTextField(titleTextField)
        TextField.setupMaterialTextField(expirationDateTextField)
        TextField.setupMaterialTextField(amountTextField)
        TextField.setupMaterialTextField(categoryTextField)
    }
    
    private func isFilled() -> Bool {
        if !titleTextField.hasText() ||
            !expirationDateTextField.hasText() ||
            !amountTextField.hasText() ||
            !categoryTextField.hasText() {
                
                UIAlertView.showMessage(AlertMessages.FieldsNotFilled)
                return false
        }
        return true
    }
    
    // MARK: - requests
    
    private func addMedicament() {
        if !isFilled() {
            return
        }
        
        let name = titleTextField.text!
        let expirationDate = expirationDateTextField.text!
        let amount = Int(amountTextField.text!)!
        let category = categoryTextField.text!
        let userId = HTTPClient.currentUserUid!
        
        let medicament = Medicament(userId: userId, name: name, expirationDate: expirationDate, amount: amount, category: category)
        
        HTTPClient.createMedicament(medicament) { (error) -> Void in
            if let error = error {
                UIAlertView.showError(error)
                return
            }
            
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    private func editMedicament() {
        if !isFilled() {
            return
        }
        
        medicament?.name = titleTextField.text!
        medicament?.expirationDate = expirationDateTextField.text!
        medicament?.amount = Int(amountTextField.text!)!
        medicament?.category = categoryTextField.text!
        
        HTTPClient.updateMedicament(medicament!) { (error) -> Void in
            if let error = error {
                UIAlertView.showError(error)
                return
            }
            
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
}

// MARK: - UITextFieldDelegate

//extension EditMedicamentViewController: UITextFieldDelegate {
//    
//    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
//        if let textField = textField as? SRKDateTimeBox {
//            textField.delegateForDateTimeBox = self
//            textField.showOptions()
//            
//            return false
//        }
//        if let textField = textField as? SRKComboBox {
//            textField.delegateForComboBox = self
//            textField.showOptions()
//            return false
//        }
//        return true
//    }
// 
//}

// MARK: - SRKComboBoxDelegate
//
//extension EditMedicamentViewController: SRKComboBoxDelegate {
//    func comboBox(textField:SRKComboBox, didSelectRow row:Int) {
//        if textField == self.medicamentsAmountTextField {
//            self.medicamentsAmountTextField.text = self.medicamentsAmountArray[row]
//        }
//    }
//    
//    func comboBoxNumberOfRows(textField:SRKComboBox) -> Int {
//        if textField == self.medicamentsAmountTextField {
//            return self.medicamentsAmountArray.count
//        } else {
//            return 0
//        }
//    }
//    
//    func comboBox(textField:SRKComboBox, textForRow row:Int) -> String {
//        if textField == self.medicamentsAmountTextField {
//            return self.medicamentsAmountArray[row]
//        } else {
//            return ""
//        }
//    }
//    
//    func comboBoxPresentingViewController(textField:SRKComboBox) -> UIViewController {
//        return self
//    }
//    
//    func comboBoxRectFromWhereToPresent(textField:SRKComboBox) -> CGRect {
//        return textField.frame
//    }
//    
//    func comboBoxFromBarButton(textField:SRKComboBox) -> UIBarButtonItem? {
//        return nil
//    }
//    
//    func comboBoxTintColor(textField:SRKComboBox) -> UIColor {
//        return UIColor.blackColor()
//    }
//    
//    func comboBoxToolbarColor(textField:SRKComboBox) -> UIColor {
//        return UIColor.whiteColor()
//    }
//    
//    func comboBoxDidTappedCancel(textField:SRKComboBox) {
//        textField.text = ""
//    }
//    
//    func comboBoxDidTappedDone(textField:SRKComboBox) {
//        print("Let's do some action here")
//    }
//
//}
//
//// MARK: - SRKDateTimeBoxDelegate
//
//extension EditMedicamentViewController: SRKDateTimeBoxDelegate {
//
//    func dateTimeBox(textField:SRKDateTimeBox, didSelectDate date:NSDate) {
//        let dateFormatter = NSDateFormatter()
//            dateFormatter.dateFormat = "dd-MMM-yyyy"
//            self.expirationDateTextField.text = dateFormatter.stringFromDate(date)
//    }
//    
//    func dateTimeBoxType(textField:SRKDateTimeBox) -> UIDatePickerMode {
//        if textField == self.expirationDateTextField {
//            return UIDatePickerMode.Date
//        } else if textField == self.expirationDateTextField {
//            return UIDatePickerMode.Time
//        } else {
//            return UIDatePickerMode.Date
//        }
//    }
//    
//    func dateTimeBoxMinimumDate(textField:SRKDateTimeBox) -> NSDate? {
//        return nil
//    }
//    
//    func dateTimeBoxMaximumDate(textField:SRKDateTimeBox) -> NSDate? {
//        return nil
//    }
//    
//    func dateTimeBoxPresentingViewController(textField:SRKDateTimeBox) -> UIViewController {
//        return self
//    }
//    
//    func dateTimeBoxRectFromWhereToPresent(textField:SRKDateTimeBox) -> CGRect {
//        return textField.frame
//    }
//    
//    func dateTimeBoxFromBarButton(textField:SRKDateTimeBox) -> UIBarButtonItem? {
//        return nil
//    }
//    
//    func dateTimeBoxTintColor(textField:SRKDateTimeBox) -> UIColor {
//        return UIColor.blackColor()
//    }
//    
//    func dateTimeBoxToolbarColor(textField:SRKDateTimeBox) -> UIColor {
//        return UIColor.whiteColor()
//    }
//    
//    func dateTimeBoxDidTappedCancel(textField:SRKDateTimeBox) {
//        textField.text = ""
//    }
//    
//    func dateTimeBoxDidTappedDone(textField:SRKDateTimeBox) {
//        print("Let's do some action here")
//    }
