//
//  AuthorizationViewController.swift
//  Aptechka
//
//  Created by Vladyslav Korneyev on 3/14/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import UIKit
import Material

class AuthorizationViewController: BaseViewController {
    
    @IBOutlet weak private var emailTextField: TextField!
    @IBOutlet weak private var passwordTextField: TextField!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTextFields()
    }
    
    // MARK: - IBActions
    
    @IBAction func signInButtonDidTap(sender: AnyObject) {
        if !isFilled() {
            return
        }
        
        let email = emailTextField.text!
        let password = passwordTextField.text!
        
        if !email.isValidEmail() {
            UIAlertView.showMessage(AlertMessages.InvalidEmail)
            return
        }
        
        HTTPClient.loginUser(email, password: password, completion: { (error) -> Void in
            if let error = error {
                UIAlertView.showError(error)
                return
            }
            
            self.performSegueWithIdentifier(Segues.PresentMainFromAuthorizeSegue, sender: nil)
        })
    }
    
    // MARK: - Private
    
    private func setupTextFields() {
        TextField.setupMaterialTextField(emailTextField)
        TextField.setupMaterialTextField(passwordTextField)
    }
    
    private func isFilled() -> Bool {
        if !emailTextField.hasText() || !passwordTextField.hasText() {
            UIAlertView.showMessage(AlertMessages.FieldsNotFilled)
            return false
        }
        return true
    }
}

// MARK: - UITextFieldDelegate

extension AuthorizationViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        (textField as! TextField).detailLabelHidden = 0 == textField.text?.utf16.count
        return false
    }

}
