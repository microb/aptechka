//
//  RegistrationViewController.swift
//  Aptechka
//
//  Created by Maxim Kamenev on 3/18/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import UIKit
import Material

class RegistrationViewController: BaseViewController {
    
    @IBOutlet weak private var emailTextField: TextField!
    @IBOutlet weak private var passwordTextField: TextField!
    @IBOutlet weak var confirmPasswordTextField: TextField!
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTextFields()
    }
    
    // MARK: - Actions
    
    @IBAction func registerButtonDidTap(sender: AnyObject) {
        if !isFilled() {
            return
        }
        
        let email = emailTextField.text!
        let password = passwordTextField.text!
        let confirmPassword = confirmPasswordTextField.text!
        
        if !email.isValidEmail() {
            UIAlertView.showMessage(AlertMessages.InvalidEmail)
            return
        }
        
        if password != confirmPassword {
            UIAlertView.showMessage(AlertMessages.PasswordDontMatch)
            return
        }
        
        HTTPClient.registerUser(email, password: password) { (error) -> Void in
            if let error = error {
                UIAlertView.showError(error)
                return
            }
            
            HTTPClient.loginUser(email, password: password, completion: { (error) -> Void in
                if let error = error {
                    UIAlertView.showError(error)
                    return
                }
                
                self.performSegueWithIdentifier(Segues.PresentMainFromRegistrationSegue, sender: nil)
            })
        }
    }
    
    // MARK: - Private
    
    private func setupTextFields() {
        TextField.setupMaterialTextField(emailTextField)
        TextField.setupMaterialTextField(passwordTextField)
        TextField.setupMaterialTextField(confirmPasswordTextField)
    }
    
    private func isFilled() -> Bool {
        if !emailTextField.hasText() || !passwordTextField.hasText() || !confirmPasswordTextField.hasText(){
            UIAlertView.showMessage(AlertMessages.FieldsNotFilled)
            return false
        }
        return true
    }
}

// MARK: - UITextFieldDelegate

extension RegistrationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        (textField as! TextField).detailLabelHidden = 0 == textField.text?.utf16.count
        return false
    }
}
