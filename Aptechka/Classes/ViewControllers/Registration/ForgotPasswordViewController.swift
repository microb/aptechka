//
//  ForgotPasswordViewController.swift
//  Aptechka
//
//  Created by Vladyslav Korneyev on 3/21/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import UIKit
import Material

class ForgotPasswordViewController: BaseViewController {

    @IBOutlet weak var eMailTextField: TextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TextField.setupMaterialTextField(eMailTextField)
    }
}
