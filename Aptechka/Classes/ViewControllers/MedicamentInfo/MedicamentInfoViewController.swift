//
//  DetailedMedicamentsViewController.swift
//  Aptechka
//
//  Created by Maxim Kamenev on 3/2/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import UIKit
import Material

class MedicamentInfoViewController: BaseViewController {
    
    @IBOutlet weak private var medicamentsAmountLabel: UILabel!
    @IBOutlet weak private var hoursCounterLabel: UILabel!
    @IBOutlet weak private var expiringDataLabel: UILabel!
    @IBOutlet weak private var medicamentNameLabel: UILabel!
    
    var medicament: Medicament!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }

    // MARK: - IBActions
    
    @IBAction func editButtonDidTap(sender: AnyObject) {
        performSegueWithIdentifier(Segues.ShowMedicamentEditingSegue, sender: medicament)
    }
    
    @IBAction func deleteButtonDidTap(sender: AnyObject) {
        deleteMedicament()
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let medicamentVC = segue.destinationViewController as! MedicamentViewController
        medicamentVC.medicament = sender as? Medicament
    }
    
    // MARK: - Private
    
    // MARK: - Requests
    
    private func deleteMedicament() {
        HTTPClient.deleteMedicament(medicament) { (error) -> Void in
            if let error = error {
                UIAlertView.showError(error)
                return
            }
            
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    private func setupUI() {
        medicamentNameLabel.text = medicament.name
        medicamentsAmountLabel.text = String(medicament.amount)
        expiringDataLabel.text = medicament.expirationDate
    }
}
