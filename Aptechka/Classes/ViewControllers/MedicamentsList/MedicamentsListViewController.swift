//
//  MedicamentsListViewController.swift
//  Aptechka
//
//  Created by Anton Muratov on 3/2/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import UIKit
import Material

class MedicamentsListViewController: BaseViewController {
    
    @IBOutlet weak private var searchBar: UISearchBar!
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var addMedicamentButton: FabButton!
    @IBOutlet weak private var menuView: MenuView!
    @IBOutlet weak private var headerContainer: UIView!
    
    private var medicaments = [Medicament]()
    
    private var lastScrollViewContentOffset: CGFloat!
    private var transition = QZCircleSegue()
    
    /// Default spacing size
    let spacing: CGFloat = 16
    
    /// Diameter for FabButtons.
    let fabButtonDiameter: CGFloat = 50
    
    /// Height for FlatButtons.
    let flatButtonHeight: CGFloat = 40
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareMenuView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchMedicaments()
    }
    
    // MARK: - IBActions
    
    @IBAction func unwindToMainViewController (sender: UIStoryboardSegue){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Segues.ShowMedicamentInfoSegue {
            let medicamentInfoVC = segue.destinationViewController as! MedicamentInfoViewController
            medicamentInfoVC.medicament = sender as! Medicament
        }
    }
    
    // MARK: - Private
    
    // MARK: - Requests
    
    private func fetchMedicaments() {
        HTTPClient.fetchMedicaments { (response, error) -> Void in
            if let error = error {
                UIAlertView.showError(error)
                return
            }
            
            if let medicamentsArray = response as? [Medicament] {
                self.medicaments = medicamentsArray
                self.tableView.reloadData()
            }
        }
    }
    
    private func logout() {
        HTTPClient.logoutUser { (error) -> Void in
            self.performSegueWithIdentifier(Segues.PresentAuthorizationSegue, sender: nil)
        }
    }
    
    // MARK: - UI
    
    private func showButton() {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.addMedicamentButton.transform = CGAffineTransformIdentity
        }
    }
    
    private func hideButton() {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.addMedicamentButton.transform = CGAffineTransformMakeTranslation(0,
                self.addMedicamentButton.bounds.size.width * 3)
        }
    }
    
    private func showSearchBar() {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.headerContainer.transform = CGAffineTransformIdentity
            self.tableView.transform = CGAffineTransformIdentity
            self.menuView.transform = CGAffineTransformIdentity
        }
    }
    
    private func hideSearchBar() {
        UIView.animateWithDuration(0.5) { () -> Void in
            self.headerContainer.transform = CGAffineTransformMakeTranslation(0, -self.headerContainer.bounds.size.height)
            self.tableView.transform = CGAffineTransformMakeTranslation(0, -44)
            self.menuView.transform = CGAffineTransformMakeTranslation(0, -44)
        }
    }
    
    // MARK: - Navigation menu
    
    dynamic private func handleMenu() {
        if menuView.menu.opened {
            (menuView.menu.views?.first as? MaterialButton)?.userInteractionEnabled = false
            UIView.animateWithDuration(0.4, animations: { () -> Void in
                self.menuView.menu.close()
                (self.menuView.menu.views?.first as? MaterialButton)?.transform = CGAffineTransformMakeRotation(0)
                }, completion: { (completion) -> Void in
                    (self.menuView.menu.views?.first as? MaterialButton)?.userInteractionEnabled = true
            })
        } else {
            menuView.menu.open() { (view: UIView) in
                (view as? MaterialButton)?.pulse()
            }
            (menuView.menu.views?.first as? MaterialButton)?.userInteractionEnabled = false
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                (self.menuView.menu.views?.first as? MaterialButton)?.transform = CGAffineTransformMakeRotation(0.5)
                }, completion: { (completion) -> Void in
                    (self.menuView.menu.views?.first as? MaterialButton)?.userInteractionEnabled = true
            })
        }
    }
    
    dynamic private func handleButton(button: UIButton) {
        print("Hit Button \(button)")
    }
    
    dynamic private func logoutButtonDidTap(button: UIButton) {
        logout()
    }
    
    dynamic private func aboutUsButtonDidTap(button: UIButton) {
        let aboutUsViewController = storyboard!.instantiateViewControllerWithIdentifier("AboutUsNavigationController")
        presentViewController(aboutUsViewController, animated: true) { () -> Void in
            print("About us VC.")
        }
    }
    
    private func prepareMenuView() {
        var image: UIImage? = UIImage(named: "menuIcon_image")
        let btn1: FlatButton = FlatButton()
        btn1.depth = .None
        btn1.tintColor = MaterialColor.white
        btn1.pulseColor = nil
        btn1.borderColor = nil
        btn1.backgroundColor = nil
        btn1.setImage(image, forState: .Normal)
        btn1.setImage(image, forState: .Highlighted)
        btn1.addTarget(self, action: Selector("handleMenu"), forControlEvents: .TouchUpInside)
        menuView.addSubview(btn1)
        
        image = UIImage(named: "menuIconOrder_image")
        let btn2: FabButton = FabButton()
        btn2.depth = .None
        btn2.pulseColor = MaterialColor.pink.accent3
        btn2.backgroundColor = MaterialColor.clear
        btn2.setBackgroundImage(image, forState: .Normal)
        btn2.setBackgroundImage(image, forState: .Highlighted)
        btn2.addTarget(self, action: Selector("handleButton:"), forControlEvents: .TouchUpInside)
        menuView.addSubview(btn2)
        
        image = UIImage(named: "menuIconLogOut_image")
        let btn3: FabButton = FabButton()
        btn3.depth = .None
        btn3.pulseColor = MaterialColor.pink.accent3
        btn3.backgroundColor = MaterialColor.clear
        btn3.setBackgroundImage(image, forState: .Normal)
        btn3.setBackgroundImage(image, forState: .Highlighted)
        btn3.addTarget(self, action: Selector("logoutButtonDidTap:"), forControlEvents: .TouchUpInside)
        menuView.addSubview(btn3)
        
        image = UIImage(named: "menuIconAboutUs_image")
        let btn4: FabButton = FabButton()
        btn4.depth = .None
        btn4.pulseColor = MaterialColor.pink.accent3
        btn4.backgroundColor = MaterialColor.clear
        btn4.setBackgroundImage(image, forState: .Normal)
        btn4.setBackgroundImage(image, forState: .Highlighted)
        btn4.addTarget(self, action: Selector("aboutUsButtonDidTap:"), forControlEvents: .TouchUpInside)
        menuView.addSubview(btn4)
        
        image = UIImage(named: "menuIconSettings_image")
        let btn5: FabButton = FabButton()
        btn5.depth = .None
        btn5.pulseColor = MaterialColor.pink.accent3
        btn5.backgroundColor = MaterialColor.clear
        btn5.setBackgroundImage(image, forState: .Normal)
        btn5.setBackgroundImage(image, forState: .Highlighted)
        btn5.addTarget(self, action: Selector("handleButton:"), forControlEvents: .TouchUpInside)
        menuView.addSubview(btn5)
        
        // Initialize the menu and setup the configuration options.
        menuView.menu.direction = .Down
        menuView.menu.baseViewSize = CGSizeMake(flatButtonHeight, flatButtonHeight)
        menuView.menu.itemViewSize = CGSizeMake(fabButtonDiameter, fabButtonDiameter)
        menuView.menu.views = [btn1, btn2, btn3, btn4, btn5]
    }

}

// MARK: - UITableViewDataSource

extension MedicamentsListViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medicaments.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(TableViewCellsIdentifiers.MedicamentCell) as! MedicamentTableViewCell
        cell.medicament = medicaments[indexPath.row]
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension MedicamentsListViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let medicament = medicaments[indexPath.row]
        performSegueWithIdentifier(Segues.ShowMedicamentInfoSegue, sender: medicament)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if lastScrollViewContentOffset > scrollView.contentOffset.y { // Scroll to tup
            showButton()
            showSearchBar()
        } else {
            hideButton()
            hideSearchBar()
            
            if menuView.menu.opened {
                menuView.menu.close()
                (menuView.menu.views?.first as? MaterialButton)?.animate(MaterialAnimation.rotate(rotation: 0))
            }
        }
        
        lastScrollViewContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
}

// MARK: - UISearchBarDelegate

extension MedicamentsListViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
