//
//  MedicamentTableViewCell.swift
//  Aptechka
//
//  Created by Anton Muratov on 3/2/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import UIKit
import Material

class MedicamentTableViewCell: MaterialTableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var expirationDateLabel: UILabel!
    
    
    var medicament: Medicament {
        didSet {
            titleLabel.text = medicament.name
            expirationDateLabel.text = medicament.expirationDate
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        medicament = Medicament()
        super.init(coder: aDecoder)
    }
}
