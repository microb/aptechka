//
//  Medicament.swift
//  Aptechka
//
//  Created by Anton Muratov on 3/2/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import UIKit
import Firebase

class Medicament: BaseObject {
    
    let id: String
    let userId: String
    
    var name: String
    var expirationDate: String
    var amount: Int
    var category: String
    
    override init() {
        self.id = String()
        self.userId = String()
        self.name = String()
        self.expirationDate = String()
        self.amount = Int()
        self.category = String()
    }
    
    init(id: String = "", userId: String, name: String, expirationDate: String, amount: Int, category: String) {
        self.id = id
        self.userId = userId
        self.name = name
        self.expirationDate = expirationDate
        self.amount = amount
        self.category = category
    }
    
    static func createWithSnapshot(snapshot: FDataSnapshot) -> Medicament? {
        guard let id = snapshot.value[Keys.Id] as? String,
            let userId = snapshot.value[Keys.UserId] as? String,
            let name = snapshot.value[Keys.Name] as? String,
            let expirationDate = snapshot.value[Keys.ExpirationDate] as? String,
            let category = snapshot.value[Keys.Category] as? String,
            let amount = snapshot.value[Keys.Amount] as? Int else { return nil }
        
        return Medicament(id: id, userId: userId, name: name, expirationDate: expirationDate,
            amount: amount, category: category)
    }
}
