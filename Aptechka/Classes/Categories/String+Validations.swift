//
//  String+Validations.swift
//  Aptechka
//
//  Created by Anton Muratov on 3/22/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import Foundation

extension String {
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let result = emailTest.evaluateWithObject(self)
        
        return result
        
    }
    
}
