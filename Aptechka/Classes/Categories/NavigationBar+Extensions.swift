//
//  NavigationBar+Extensions.swift
//  Aptechka
//
//  Created by Anton Muratov on 3/7/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func setBottomBorderColor(color: UIColor, height: CGFloat) {
        let bottomBorderRect = CGRectMake(0, CGRectGetHeight(frame), CGRectGetWidth(frame), height)
        let bottomBorder = UIView.init(frame: bottomBorderRect)
        bottomBorder.backgroundColor = color
        self.addSubview(bottomBorder)
    }
}
