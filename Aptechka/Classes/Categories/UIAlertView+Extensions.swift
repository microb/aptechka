//
//  UIAlertView+Extensions.swift
//  MobileCommunity
//
//  Created by Anton Muratov on 2/19/16.
//  Copyright © 2016 Micro-b. All rights reserved.
//

import UIKit

extension UIAlertView {
    
    static func showMessage(message: String) {
        UIAlertView(
            title: nil,
            message: message,
            delegate: nil,
            cancelButtonTitle: "OK").show()
    }
    
    static func showMessageWithTitle(title: String!, message: String!) {
        UIAlertView (
            title: title,
            message: message,
            delegate: nil,
            cancelButtonTitle: "OK").show()
    }
    
    static func showError(error: NSError) {
        UIAlertView(
            title: "Error!",
            message: error.localizedDescription,
            delegate: nil,
            cancelButtonTitle: "OK").show()
    }
}
