//
//  TextField+Material.swift
//  Aptechka
//
//  Created by Maxim Kamenev on 3/21/16.
//  Copyright © 2016 micro-b. All rights reserved.
//

import UIKit
import Material

extension TextField {

    class func setupMaterialTextField(textField: TextField) {
        textField.placeholderTextColor = MaterialColor.grey.base
        textField.font = RobotoFont.regularWithSize(20)
        textField.textColor = MaterialColor.black
        textField.borderStyle = .None
        
        textField.titleLabel = UILabel()
        textField.titleLabel!.font = RobotoFont.mediumWithSize(12)
        textField.titleLabelColor = MaterialColor.grey.base
        textField.titleLabelActiveColor = UIColor.colorWithHexString(Colors.NavigationBarColor)
        textField.tintColor = UIColor.colorWithHexString(Colors.NavigationBarColor)
     }

}